package com.pupscan.chains.store

import com.pupscan.chains.stream.Streamizer
import java.io.File

/**
 * Simple Pojo disk store based on a DiskStreamStore.
 * Well suited for low frequency access (typically every 5 sec).
 * Because there is no in-memory caching, it should not be used as a high freq caching system.
 */
class DiskStore<T>(val directory: File, val streamizer: Streamizer<T>) : Store<T> {

    private val diskStreamStore = DiskStreamStore(directory, streamizer.extensions())

    override fun put(key: String, t: T) {
        diskStreamStore.put(key, streamizer.stream(t))
    }


    override fun get(key: String): T {
        return streamizer.unstream(diskStreamStore.get(key))
    }

    override fun find(key: String): T? {
        try {
            return get(key)
        } catch (e: Store.NotFoundException) {
            return null
        }
    }

    override fun delete(key: String) {
        diskStreamStore.delete(key)
    }

    override fun list(): List<String> {
        return diskStreamStore.list()
    }

    override fun getAll(): Sequence<Pair<String, T>> {
        return diskStreamStore.list().asSequence().map { it to get(it) }
    }

    override fun clear() {
        diskStreamStore.clear()
        directory.deleteRecursively()
    }
}