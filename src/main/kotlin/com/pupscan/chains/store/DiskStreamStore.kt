package com.pupscan.chains.store

import org.slf4j.LoggerFactory
import java.io.File
import java.io.FilenameFilter
import java.io.InputStream

/**
 * A disk Store for items of type List<InputStream> which can suit to any "big" object serialization.
 * File writes are transactional.
 *
 * EXAMPLE of directory structure :
 *
 * directory
 *  |- key1-00.pdf
 *  |- key1-01.json
 *  |- key2-00.pdf
 *  |- key2-01.json
 *
 *
 */
class DiskStreamStore(private val directory: File, extensions: List<String>) : Store<List<InputStream>> {
    private val separator = "_._"
    private val tempFilePrefix = ".tmp."
    private val extensionsNoDot: List<String>
    private val streamIndexPadding: Int

    init {
        directory.mkdirs()
        if (extensions.isEmpty()) throw Exception("Extensions list cannot be empty")
        cleanTempFiles()
        extensionsNoDot = cleanExtensionList(extensions)
        streamIndexPadding =
            if (extensionsNoDot.size <= 10) 1
            else if (extensionsNoDot.size <= 100) 2
            else throw IndexOutOfBoundsException(
                "Attempt to create a store to handle ${extensionsNoDot.size} streams per entry." +
                        "But it does not make sense to store more than 100 using this store architecture."
            )
    }

    override fun list(): List<String> {
        return findFilesWithFilters().map {
            it.name.split(separator)[0]
        }.distinct()
    }


    /**
     * Will overwrite any existing stream.
     */
    override fun put(key: String, t: List<InputStream>) {
        validateListSize(t)
        t.forEachIndexed { streamIndex, stream ->
            putStream(key, streamIndex, stream, extensionsNoDot[streamIndex])
        }
    }

    override fun get(key: String): List<InputStream> {
        val streams = mutableListOf<InputStream>()
        for (streamIndex in 0 until extensionsNoDot.size) {
            val file = streamFile(key, streamIndex, extensionsNoDot[streamIndex])
            if (streamIndex == 0 && !file.exists()) {
                throw Store.NotFoundException(key)
            }
            streams.add(file.inputStream())
        }
        return streams
    }

    override fun find(key: String): List<InputStream>? {
        try {
            return get(key)
        } catch (e: Store.NotFoundException) {
            return null
        }
    }

    override fun getAll(): Sequence<Pair<String, List<InputStream>>> {
        return list().asSequence().map { it to get(it) }
    }

    override fun delete(key: String) {
        return findFiles(key).forEach { it.delete() }
    }

    override fun clear() {
        directory.deleteRecursively()
    }

    /**
     * Should be used on startup to drop broken artifacts that might have been
     * left by previous run.
     */
    private fun cleanTempFiles() {
        return findTempFiles().forEach { it.delete() }
    }

    private fun cleanExtensionList(exts: List<String>): List<String> {
        val extensionsNoDot = mutableListOf<String>()
        exts.forEach {
            extensionsNoDot.add(it.removePrefix("."))
        }
        return extensionsNoDot
    }

    private fun validateListSize(list: List<InputStream>) {
        if (list.size != extensionsNoDot.size) {
            throw Exception("Expecting a list of size ${extensionsNoDot.size}, but was of size ${list.size}.")
        }
    }

    private fun putStream(key: String, streamIndex: Int, stream: InputStream, extNoDot: String) {
        val file = streamFile(key, streamIndex, extNoDot)
        file.delete()
        val tmpFile = tempFile(file)
        try {
            directory.mkdirs()
            tmpFile.outputStream().use {
                stream.copyTo(it)
            }
            tmpFile.renameTo(file)
        } catch (exception: Exception) {
            throw Exception("Cannot put stream[$key.$streamIndex] to file '$file'.", exception)
        }
    }

    private fun streamFile(key: String, streamIndex: Int, extNoDot: String): File {
        return File(directory, prefix(key, streamIndex) + ".$extNoDot")
    }

    private fun tempFile(file: File): File {
        return File(file.parent, "$tempFilePrefix${file.name}")
    }

    private fun prefix(key: String, streamIndex: Int): String {
        val paddedStreamIndex = streamIndexToString(streamIndex)
        return "$key$separator$paddedStreamIndex"
    }

    private fun streamIndexToString(streamIndex: Int): String {
        return streamIndex.toString().padStart(streamIndexPadding, '0')
    }

    private fun findTempFiles(): List<File> {
        return findFilesWithFilters(tempFilePrefix)
    }

    private fun findFiles(key: String): List<File> {
        return findFilesWithFilters("$key$separator")
    }


    private fun findFilesWithFilters(prefix: String = "", suffix: String = ""): List<File> {
        val names = directory.list(object : FilenameFilter {
            override fun accept(dir: File?, name: String?): Boolean {
                if (name == null) return false
                val startsWith = name.startsWith(prefix, false)
                val endsWith = name.endsWith(suffix, false)
                return startsWith && endsWith
            }
        })
        if (names == null) return emptyList()
        return names
            .sorted()
            .map { File(directory, it) }
    }

    private data class Entry(val key: String, val index: Int, val streamIndex: Int)
}