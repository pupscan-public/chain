package com.pupscan.chains.processor

class History(private val maxHistorySize: Int = 20) {

    private val events = mutableListOf<Event>()


    data class Event(val time: Long, val completed: Int)

    fun put(time: Long, completed: Int) {
        if (events.size == maxHistorySize) {
            events.removeAt(0)
        }
        events.add(Event(time, completed))
    }

    fun count(): Int {
        return events.count()
    }

    fun delaySinceLastEvent(): Long {
        return System.currentTimeMillis() - lastOrOldest().time
    }

    fun thirdLastOrOldest(): Event {
        return nThLastOrOldest(3)
    }

    fun secondLastOrOldest(): Event {
        return nThLastOrOldest(2)
    }

    fun lastOrOldest(): Event {
        return nThLastOrOldest(1)
    }

    fun nThLastOrOldest(nLast: Int): Event {
        if (events.size >= nLast)
            return events[events.size - nLast]
        else if (events.size > 0)
            return events[0]
        else
            return Event(System.currentTimeMillis(), 0)
    }
}