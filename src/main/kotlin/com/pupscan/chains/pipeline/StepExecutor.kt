package com.pupscan.chains.pipeline

import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.condition.Condition
import com.pupscan.chains.processor.ChainProcessor
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * An independant multi-threaded execution unit for running submitted Processors for a certain Step.
 */
class StepExecutor(
    val step: String = "stepName",
    nThread: Int = 2,
    private val conditionExecutor: ListeningExecutorService = Condition.DEFAULT_EXECUTOR
) {

    private val log = LoggerFactory.getLogger(javaClass)
    private val executor = MoreExecutors.listeningDecorator(
        Executors.newFixedThreadPool(
            nThread,
            ThreadFactoryBuilder()
                .setNameFormat("executor-$step-%d")
                .setUncaughtExceptionHandler { thread, e ->
                    log.error("UncaughtException in thread ${thread.name}.", e)
                }
                .build()
        )
    )

    /**
     * Terminate running and queued tasks and Shutdown executor.
     */
    fun shutdownWhenDone() {
        log.debug("[$step] Shutdown initiated.")
        executor.shutdown()
    }

    /**
     * Interrupt running tasks and Shutdown
     */
    fun shutdownNow() {
        log.debug("[$step] Interrupting Processors in progress.")
        executor.shutdownNow()
    }

    fun awaitTermination(timeoutSec: Int = Int.MAX_VALUE): Boolean {
        log.debug("[$step] Waiting for Processors to finish.")
        val success = executor.awaitTermination(timeoutSec.toLong(), TimeUnit.SECONDS)
        log.debug(if (success) "[$step] Terminated" else "Not terminated, timeout $timeoutSec sec.")
        return success
    }

    fun submit(
        chainProcessor: ChainProcessor, condition: Condition, successObserver: () -> Unit = {},
        errorObserver: (Exception) -> Unit = {}
    ) {
        log.debug("[$step] Submit $chainProcessor.")
        val endHandler = ProcessorEndHandler(chainProcessor, condition, successObserver, errorObserver)
        recSubmit(chainProcessor, endHandler)
    }

    private fun recSubmit(chainProcessor: ChainProcessor, endHandler: ProcessorEndHandler) {
        executor.submit<ChainProcessor> {
            log.debug("[$step] (Re)starting $chainProcessor.")
            chainProcessor.run()
            chainProcessor
        }.addListener(endHandler, executor)
    }

    inner class ProcessorEndHandler(
        private val chainProcessor: ChainProcessor,
        private val condition: Condition,
        private val successObserver: () -> Unit,
        private val errorObserver: (Exception) -> Unit = {}
    ) :
        Runnable {
        override fun run() {
            log.debug("[$step] End Handling $chainProcessor.")
            when (chainProcessor.state) {
                ChainProcessor.State.SUCCESS -> successObserver()
                ChainProcessor.State.ERROR -> {
                    when (val e = chainProcessor.progress.exception) {
                        is InterruptedException -> {
                            errorObserver(e)
                            Unit
                        }
                        is Chain.IllegalReadException -> {
                            errorObserver(e)
                            Unit
                        }
                        is TimeoutException -> {
                            condition.runWhenVerified(Runnable { recSubmit(chainProcessor, this) })
                        }
                        else -> {
                            (chainProcessor.retryCondition + condition).runWhenVerified(Runnable {
                                recSubmit(chainProcessor, this)
                            })
                        }
                    }
                }
                else -> Unit
            }
        }
    }

}