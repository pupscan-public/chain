package com.pupscan.chains.condition

import com.google.common.util.concurrent.ListenableScheduledFuture
import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.pupscan.chains.processor.History
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * A Condition based on a delay computed from the the ChainProcessor execution history.
 * It is used to determine if we have been waiting long "enough" to allow a new retry.
 * The only strategy implemented so far is Fibonacci.
 */
class RetryDelayCondition(
    private val strategy: Strategy = Strategy.WaitForDuration(),
    executor: ListeningExecutorService = DEFAULT_EXECUTOR
) : Condition(executor) {

    private val log = LoggerFactory.getLogger(javaClass)
    private var future: ListenableScheduledFuture<*>? = null


    override fun triggerOrRegister() {
        if (future != null) return
        val delay = strategy.delayMsBeforeRetry()
        log.debug("Scheduling next retry in $delay ms.")
        future = scheduler.schedule({
            trigger()
        }, delay, TimeUnit.MILLISECONDS)
    }

    override fun unregister() {
        future?.cancel(false)
        future = null
    }

    fun retryNow() {
        trigger()
    }

    interface Strategy {
        fun delayMsBeforeRetry(): Long

        class WaitForDuration(val durationMs: Long = 5000L) : Strategy {
            override fun delayMsBeforeRetry(): Long {
                return durationMs
            }
        }

        class Fibonacci(private val history: History) : Strategy {
            override fun delayMsBeforeRetry(): Long {

                val thirdLastEvent = history.thirdLastOrOldest()
                val secondLastEvent = history.secondLastOrOldest()
                val lastEvent = history.lastOrOldest()

                val secondLastWait = secondLastEvent.time - thirdLastEvent.time
                val lastWait = lastEvent.time - secondLastEvent.time

                val waitCommand =
                    if (lastEvent.completed > secondLastEvent.completed)
                    //Last try was partially successful because progress changed
                        0L
                    else
                        if (secondLastEvent.completed > thirdLastEvent.completed)
                        //This is our first retry
                            lastWait
                        else
                        //This is at least our second retry
                            secondLastWait + lastWait


                val actualWait = history.delaySinceLastEvent()
                return if (waitCommand < actualWait) 0L
                else waitCommand - actualWait
            }
        }
    }


    companion object {
        private val scheduler = MoreExecutors.listeningDecorator(
            Executors.newSingleThreadScheduledExecutor(
                ThreadFactoryBuilder().setNameFormat("condition-scheduler-%d").build()
            )
        )
    }
}