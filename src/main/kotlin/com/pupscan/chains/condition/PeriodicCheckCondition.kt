package com.pupscan.chains.condition

import com.google.common.util.concurrent.ListenableScheduledFuture
import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * A Condition based on checking validity periodically.
 */
class PeriodicCheckCondition(
    val isVerified: () -> Boolean,
    val checkDelayMs: Long,
    executor: ListeningExecutorService = DEFAULT_EXECUTOR
) : Condition(executor) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Volatile
    private var future: ListenableScheduledFuture<*>? = null


    override fun triggerOrRegister() {
        if (isVerified()) {
            log.debug("Trigger ${javaClass.simpleName} register skipped.")
            trigger()
        } else {
            if (future != null) return
            future = scheduler.scheduleAtFixedRate(Runnable {
                if (isVerified()) {
                    trigger()
                }
            }, 0L, checkDelayMs, TimeUnit.MILLISECONDS)
            log.debug("Trigger ${javaClass.simpleName} registered.")
        }
    }

    override fun unregister() {
        future?.cancel(false)
        log.debug("Trigger ${javaClass.simpleName} unregistered.")
        future = null
    }

    companion object {
        private val scheduler = MoreExecutors.listeningDecorator(
            Executors.newSingleThreadScheduledExecutor(
                ThreadFactoryBuilder().setNameFormat("condition-scheduler-%d").build()
            )
        )
    }
}
