package com.pupscan.chains.condition

import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Executors

/**
 * Provide an easy way to implement you Trigger logic without caring of the multi-thread constraints.
 * This class is using an internal Thread so its abstract methods are guarantied to be call from one single Thread.
 *
 *  @param executor you would like to use for the Runnable(s) when condition is verified
 */
abstract class Condition(
    protected val executor: ListeningExecutorService = DEFAULT_EXECUTOR
) {

    private val runnableQueue = ConcurrentLinkedQueue<Runnable>()

    operator fun plus(c2: Condition) = ThenCondition(this, c2)


    protected fun trigger() {
        internalStateExecutor.submit {
            var runnable: Runnable?
            while (true) {
                runnable = runnableQueue.poll()
                if (runnable == null) break
                else executor.submit(runnable)
            }
            unregister()
        }
    }


    /**
     * Register a Runnable to run when the condition is verified.
     */
    fun runWhenVerified(runnable: Runnable) {
        internalStateExecutor.submit {
            runnableQueue.add(runnable)
            triggerOrRegister()
        }
    }

    /**
     * Unregister the Runnable if inside the queue
     */
    fun cancel(runnable: Runnable) {
        internalStateExecutor.submit {
            runnableQueue.remove(runnable)
            if (runnableQueue.isEmpty()) {
                unregister()
            }
        }
    }

    /**
     * Unregister all the Runnable objects still inside the queue
     */
    fun cancelAll() {
        internalStateExecutor.submit {
            runnableQueue.clear()
            unregister()
        }
    }

    /**
     * Fire trigger() function or register the trigger() function to be invoked when the condition is verified.
     * Calling trigger() will automatically invoke unregister() so you don't have to handle it
     * by yourself.
     * When this function is fired the event will be dispatched to every Runnable
     * in the queue using the Executor.
     * It is guarantied both the functions triggerOrRegister() and unregister()
     * are called from the same thread.
     * These methods must be non-blocking and return as soon as possible.
     */
    protected abstract fun triggerOrRegister()

    protected abstract fun unregister()


    companion object {
        val DEFAULT_EXECUTOR by lazy {
            MoreExecutors.listeningDecorator(
                Executors.newSingleThreadExecutor(ThreadFactoryBuilder().setNameFormat("condition-executor-%d").build())
            )
        }


        /**
         * Shared SingleThreadExecutor in charge of manipulating instances state in a safe exclusive way.
         */
        private val internalStateExecutor = MoreExecutors.listeningDecorator(
            Executors.newSingleThreadExecutor(
                ThreadFactoryBuilder().setNameFormat("condition-trigger-%d").build()
            )
        )
    }

}


