package com.pupscan.chains.condition

import com.google.common.util.concurrent.ListeningExecutorService
import org.slf4j.LoggerFactory

/**
 * A Condition based on a sequence of two consecutive conditions.
 * 'Condition is met' when 'c1 is met' THEN 'c2 is met'
 */
class ThenCondition(
    private val c1: Condition,
    private val c2: Condition,
    executor: ListeningExecutorService = DEFAULT_EXECUTOR
) : Condition(executor) {

    private val log = LoggerFactory.getLogger(javaClass)


    override fun triggerOrRegister() {
        c1.runWhenVerified(Runnable {
            c2.runWhenVerified(Runnable { trigger() })
        })
    }

    override fun unregister() {
    }
}