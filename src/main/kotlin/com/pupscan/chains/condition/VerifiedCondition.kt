package com.pupscan.chains.condition

import com.google.common.util.concurrent.ListeningExecutorService
import org.slf4j.LoggerFactory

/**
 * An always verified condition.
 * Acting as the unary element of our algebra.
 */
class VerifiedCondition(executor: ListeningExecutorService = DEFAULT_EXECUTOR) : Condition(executor) {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun triggerOrRegister() {
        trigger()
    }

    override fun unregister() {
    }
}