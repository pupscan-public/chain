package com.pupscan.chains.tool

class Chrono {

    @Volatile
    private var start: Long = 0L

    init {
        reset()
    }

    fun reset() {
        start = System.currentTimeMillis()
    }

    fun ellapsedMillis(): Long {
        return System.currentTimeMillis() - start
    }

    fun ellapsedSec(): Int {
        return (ellapsedMillis() / 1000).toInt()
    }
}