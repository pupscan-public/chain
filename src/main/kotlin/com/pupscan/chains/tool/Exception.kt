package com.pupscan.chains.tool

fun Exception.toShortString(): String {
    return javaClass.simpleName.removeSuffix("Exception") + "[$message]"
}