package com.pupscan.chains.chain

import com.pupscan.chains.store.Store

/**
 * A Chain backed by a Store to survive reboot.
 * Writes are synchronously committed to the store.
 * At startup the Chain is restored from the Store.
 */
class RestorableChain<T>(private val chain: Chain<T>, private val endElement: T, private val store: Store<Chain.Node<T>>) :
    Chain<T> {

    override val count: Int
        @Synchronized
        get() = chain.count

    override val trimCount: Int
        @Synchronized
        get() = chain.trimCount

    override val lastNode: Chain.Node<T>?
        get() = chain.lastNode

    init {
        initRestoreStateFromStore()
    }

    private fun initRestoreStateFromStore() {
        val storedNodes = store.getAll()
        storedNodes.forEachIndexed { index, (key, node) ->
            if (index == 0) {
                val trimCount = Key.toIndex(key)
                chain.initTrim(trimCount)
            }
            if (node.lazy.value != endElement)
                chain.add(node.originIndex, node.originCount, node.lazy.value)
            else
                chain.end()
        }

    }

    @Synchronized
    override fun add(originIndex: Int, originCount: Int, t: T) {
        if (isEnded()) throw IllegalStateException("Cannot add when isEnded.")
        val node = Chain.Node(originIndex, originCount, lazy { t })
        store.put(Key.forPosition(chain.count), node)
        chain.add(originIndex, originCount, t)
    }

    @Synchronized
    override fun trim(count: Int): List<Chain.Node<T>> {
        val secondLastTrimCount = chain.trimCount
        val trimedItems = chain.trim(count)
        for (pos in secondLastTrimCount until count) {
            store.delete(Key.forPosition(pos))
        }
        return trimedItems
    }

    override fun initTrim(count: Int) {
        chain.initTrim(count)
    }


    @Synchronized
    override fun end() {
        store.put(Key.forPosition(chain.count), Chain.Node(chain.count, 0, lazy { endElement }))
        chain.end()
    }

    override fun isEnded(): Boolean {
        return chain.isEnded()
    }

    @Synchronized
    override fun recycle() {
        store.clear()
        chain.recycle()
    }

    override val readPosition: Int
        get() = chain.readPosition

    override fun read(timeoutMs: Long): Chain.Node<T>? {
        return chain.read(timeoutMs)
    }

    override fun readPosition(index: Int) {
        chain.readPosition(index)
    }

    class Key() {

        companion object {

            private val indexPadding = 4
            private val maxIndex = (Math.pow(10.0, indexPadding.toDouble()) - 1).toInt()

            fun forPosition(index: Int): String {
                if (index > maxIndex) throw IllegalArgumentException("Index must not exceed $indexPadding digits.")
                return index.toString().padStart(indexPadding, '0')
            }

            fun toIndex(key: String): Int {
                return key.toInt()
            }

            fun forEnd(): String {
                return Key.forPosition(maxIndex)
            }
        }
    }
}