package com.pupscan.chains.chain

/**
 * An ordered and "endable" sequence of items T wrapped into Node<T>.
 * The Chain can be read and rewound again.
 * The Chain can be ended by invoking end().
 * Objects that will not be read again can be released using trim().
 * The upper limit for reading is writeCount.
 * The lower limit for reading is trimCount.
 * read() is blocking if the Chain is not ended until new items to be written.
 * A Timeout strategy allows to wait only for a certain duration.
 */
interface Chain<T> {
    data class Node<out T>(val originIndex: Int, val originCount: Int, val lazy: Lazy<T>)

    class IllegalReadException : RuntimeException {
        constructor(message: String, ex: Exception?) : super(message, ex) {}
        constructor(message: String) : super(message) {}
        constructor(ex: Exception) : super(ex) {}
    }

    /**
     * The readPosition that will be used for next read(...).
     */
    val readPosition: Int

    /**
     * The total number of items that were removed from the head of this buffer so far.
     */
    val trimCount: Int

    /**
     * The total number of items that were added so far (including trimmed items).
     */
    val count: Int

    /**
     * The last added node or null if none.
     */
    val lastNode: Node<T>?

    /**
     * @return : the next item, or null.
     * Null will be returned when isEnded == true and readPosition == writeCount.
     * This operation will be blocking if !isEnded until new items are added.
     *
     * @throws TimeoutException if out of time.
     * @throws InterruptedException if interrupted while waiting
     */
    fun read(timeoutMs: Long = Long.MAX_VALUE): Node<T>?

    /**
     * @param index : set the index for the next read() operation.
     * Should always succeed or throw an exception.
     *
     * @throws TimeoutException
     * @throws InterruptedException
     * @throws IllegalReadException
     */
    fun readPosition(index: Int)

    /**
     * Add a new item to this.
     * @param originIndex: the first index in the origin Chain used to produce this item
     * @param originCount: the number of items from the origin Chain used to produce this item
     */
    fun add(originIndex: Int, originCount: Int, t: T)

    /**
     * Remove additional items from the head of this Chain.
     * @return the node that where removed.
     * @throws IllegalReadException if readPosition <= count
     */
    fun trim(count: Int): List<Node<T>>

    /**
     * Used at init ONLY to simulate a passed trim.
     * This should be used for persistence when resuming to a certain trim state.
     */
    fun initTrim(count: Int)


    /**
     * Mark this as Ended. Meaning not more add() should be performed.
     */
    fun end()

    fun isEnded(): Boolean

    fun readEnded(): Boolean {
        return isEnded() && readPosition == count
    }

    fun isEmpty(): Boolean {
        return count == 0
    }


    /**
     * Free the resources.
     */
    fun recycle()
}

val <T> Chain<T>.nextOriginIndex: Int
    get() {
        val lastNode = this.lastNode ?: return 0
        return lastNode.originIndex + lastNode.originCount
    }


/**
 * A sequence reading into this Chain until the end.
 */
fun <T> Chain<T>.sequence(timeoutSec: Long = 60_000L): Sequence<Chain.Node<T>> {

    return generateSequence {
        this.read(timeoutSec)
    }
}

/**
 * Cast this into a Chain<Any>.
 */
fun <T> Chain<T>.generic(): GenericChain<T> {
    return GenericChain(this)
}

/**
 * Wrap a Chain<T> into a Chain<Any> to enter a Pipeline.
 */
@Suppress("UNCHECKED_CAST")
class GenericChain<T>(val chain: Chain<T>) : Chain<Any> {

    override val trimCount: Int
        get() = chain.trimCount
    override val readPosition: Int
        get() = chain.readPosition
    override val count: Int
        get() = chain.count

    override val lastNode: Chain.Node<Any>?
        get() = chain.lastNode?.let { Chain.Node(it.originIndex, it.originCount, lazy { it.lazy.value as Any }) }

    override fun read(timeoutMs: Long): Chain.Node<Any>? {
        val node = chain.read(timeoutMs) ?: return null
        return Chain.Node(node.originIndex, node.originCount, lazy { node.lazy.value as Any })
    }

    override fun readPosition(index: Int) {
        chain.readPosition(index)
    }

    override fun trim(count: Int): List<Chain.Node<Any>> {
        return chain
            .trim(count)
            .map { node -> Chain.Node(node.originIndex, node.originCount, lazy { node.lazy.value as Any }) }
            .toList()
    }

    override fun initTrim(count: Int) {
        chain.initTrim(count)
    }

    override fun isEnded(): Boolean {
        return chain.isEnded()
    }

    override fun recycle() {
        chain.recycle()
    }

    override fun add(originIndex: Int, originCount: Int, t: Any) {
        chain.add(originIndex, originCount, t as T)
    }

    override fun end() {
        chain.end()
    }
}