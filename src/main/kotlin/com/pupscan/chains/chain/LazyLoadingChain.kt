package com.pupscan.chains.chain

import com.pupscan.chains.store.Store

/**
 * A Chain keeping only lazy references to object into the memory.
 * The reference Index is using a Chain of string keys.
 * Objects are lazy loaded from the Store when needed.
 * The Index itself is also using a Store to survive reboot.
 * An integrity check between the Store and the Index is performed at init.
 */
class LazyLoadingChain<T>(indexStore: Store<Chain.Node<String>>, private val store: Store<T>) : Chain<T> {
    private val endKey = "END"
    private val index = RestorableChain(BufferChain(endKey), endKey, indexStore)

    /**
     * If true, calling end() will trigger recyle() and thus destroy the store.
     * Can be useful for the final Chain<Void> in a pipeline.
     */
    var autoRecycle = false

    init {
        initCheckIntegrity()
    }

    private fun initCheckIntegrity() {
        val storeSize = store.list().size
        val indexSize = index.count - index.trimCount
        if (storeSize != indexSize) {
            recycle()
            throw Exception("Integrity issue found : storeSize=$storeSize,  indexSize=$indexSize.")
        }
    }


    override val count: Int
        get() = index.count

    override val trimCount: Int
        get() = index.trimCount

    override val lastNode: Chain.Node<T>?
        get() = index.lastNode?.let { Chain.Node(it.originIndex, it.originCount, lazy { store.get(it.lazy.value) }) }

    @Synchronized
    override fun add(originIndex: Int, originCount: Int, t: T) {
        val key = index.count.toString()
        store.put(key, t)
        index.add(originIndex, originCount, key)
    }

    @Synchronized
    override fun trim(count: Int): List<Chain.Node<T>> {
        val trimmed = index.trim(count)
        for (key in trimmed) {
            store.delete(key.lazy.value)
        }
        return trimmed.map {
            Chain.Node(it.originIndex,
                it.originCount,
                lazy { throw Exception("Access to trimmed Node.item is not supported.") }
            )
        }
    }

    override fun initTrim(count: Int) {
        index.initTrim(count)
    }


    override fun end() {
        index.end()
        if (autoRecycle) {
            recycle()
        }
    }

    override fun isEnded(): Boolean {
        return index.isEnded()
    }

    override fun recycle() {
        index.recycle()
        store.clear()
    }

    override val readPosition: Int
        get() = index.readPosition

    override fun read(timeoutMs: Long): Chain.Node<T>? {
        val keyNode = index.read(timeoutMs)
        if (keyNode == null) return null
        return Chain.Node(keyNode.originIndex, keyNode.originCount, lazy { store.get(keyNode.lazy.value) })
    }

    override fun readPosition(index: Int) {
        this.index.readPosition(index)
    }
}