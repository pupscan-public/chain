package com.pupscan.chains.chain

import java.util.concurrent.atomic.AtomicBoolean

class RecycleLock(val chain: Chain<*>) {
    private val released = AtomicBoolean(false)

    /**
     * Set the recycle status to true.
     * @return false if this is the first invocation, true otherwise.
     */
    fun getAndSetRecycled(): Boolean {
        return released.getAndSet(true)
    }

    /**
     * @throws IllegalStateException when already recycled.
     */
    fun ensureNotRecycled() {
        if (released.get()) {
            throw IllegalStateException("${chain.javaClass.simpleName} is already released.")
        }
    }
}