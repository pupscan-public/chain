package com.pupscan.chains.stream

import java.io.InputStream

class InputStreamizer(private val ext: String) : Streamizer<InputStream> {

    override fun extensions(): List<String> {
        return listOf(ext)
    }

    override fun stream(t: InputStream): List<InputStream> {
        return listOf(t)
    }

    override fun unstream(streams: List<InputStream>): InputStream {
        return streams[0]
    }
}