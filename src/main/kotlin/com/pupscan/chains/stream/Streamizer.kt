package com.pupscan.chains.stream

import java.io.InputStream

interface Streamizer<T> {

    fun extensions(): List<String>

    fun stream(t: T): List<InputStream>

    fun unstream(streams: List<InputStream>): T
}