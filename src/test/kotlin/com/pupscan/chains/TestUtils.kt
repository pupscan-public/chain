package com.pupscan.chains


import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Date


object TestUtils {

    fun testDirectory(name: String): File {
        return File("build/test/$name")
    }

    fun <T> testDirectory(javaClass: Class<T>): File {
        return testDirectory(javaClass.simpleName)
    }

    fun readResourceBytes(path: String): ByteArray {
        return javaClass.getResourceAsStream(path).readBytes()
    }

    fun testImageFileName(extensionWithoutDot: String, testName: String = stackParentMethod(4)): String {
        return "$testName-${humanDate()}.$extensionWithoutDot"
    }

    fun newFile(extensionWithoutDot: String, testName: String = stackParentMethod(4)): File {
        return File("/sdcard", testImageFileName(extensionWithoutDot, testName))
    }

    fun currentMethodName(): String {
        return stackParentMethod(4)
    }

    private fun stackParentMethod(stackIndex: Int): String {
        val stacktrace = Thread.currentThread().stackTrace
        val e = stacktrace[stackIndex]//maybe this stackIndex needs to be corrected
        return e.methodName
    }

    fun humanDate(): String {
        return SimpleDateFormat("yyyyMMdd-HHmmss.SSS").format(Date())
    }
}

val timestampFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS")

fun timestamp(): String {
    val formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS")
    return LocalDateTime.now().format(formatter)
}

/** Prints the given [message] and the line separator to the standard output stream.
 * A prefix is injected with a com.pupscan.chains.timestamp.
 */
fun printlnTimed(message: Any?) {
    System.out.println("${timestamp()} : $message")
}

/** Prints the given [message] and the line separator to the standard output stream.
 * A prefix is injected with a com.pupscan.chains.timestamp.
 */
fun printlnTimed(message: Any?, e: Exception) {
    val stackTrace = StringWriter()
    e.printStackTrace(PrintWriter(stackTrace))
    System.out.println("${timestamp()} : $message\n\t$stackTrace")
}


fun Int.paddedString(maxCount: Int): String {
    return this.toString().padStart(maxCount.toString().length, '0')
}