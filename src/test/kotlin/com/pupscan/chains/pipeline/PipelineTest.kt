package com.pupscan.chains.pipeline

import com.pupscan.chains.chain.BufferChain
import com.pupscan.chains.chain.generic
import com.pupscan.chains.processor.ChainProcessor
import com.pupscan.chains.processor.SegmentProcessorTest
import org.junit.Assert
import org.junit.Test
import org.slf4j.LoggerFactory
import java.util.concurrent.Semaphore

class PipelineTest {

    val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun testResumeOnFirstPipelineCompleted() {
        val shutdownPermit = Semaphore(0)
        val STEP1 = "STEP1"
        val STEP2 = "STEP2"

        val firstProcessor = SegmentProcessorTest.RandomLineAppenderProcessor(2)
        val firstProcessorCompleted = Semaphore(0)
        val secondProcessor = SegmentProcessorTest.BlockableAppenderProcessor(2)
        secondProcessor.blocked = true

        val stateObserver = { pipeline: Pipeline, step: String ->
            val processor = pipeline.processorFor(step)
            log.debug("Observing : $processor")
            processor.progress.exception?.let {
                if (!(it is InterruptedException)) {
                    it.printStackTrace()
                    Assert.fail()
                }
            }
            if (step == STEP1 && processor.state == ChainProcessor.State.SUCCESS) {
                firstProcessorCompleted.release()
            }
            Unit
        }

        val endHandler = {
            shutdownPermit.release()
            log.debug("Observing : SUCCESS")
        }

        val src = BufferChain("end").generic()
        val intermediate = BufferChain("end").generic()
        val target = BufferChain("end").generic()


        for (i in 0..10)
            src.add(i, 1, "value$i")
        src.end()

        val pipeline = Pipeline(
            listOf(STEP1, STEP2),
            listOf(src, intermediate, target),
            listOf(
                firstProcessor,
                secondProcessor
            ),
            endHandler,
            stateObserver
        )

        val pipelineExecutor = PipelineExecutor()
        val id = "id"
        pipelineExecutor.begin(id, pipeline)

        firstProcessorCompleted.acquire()
        log.debug("Canceling Pipeline.")
        pipelineExecutor.interrupt(id)

        log.debug("Resuming Pipeline.")
        secondProcessor.blocked = false
        pipelineExecutor.begin(id, pipeline)


        shutdownPermit.acquire()
        pipelineExecutor.shutdown()
    }

    @Test
    fun testResumeOnExistingTrimmedPipeline() {
        val shutdownPermit = Semaphore(0)

        val stateObserver = { pipeline: Pipeline, step: String ->
            val processor = pipeline.processorFor(step)
            log.debug("Observing : $processor")
            processor.progress.exception?.let {
                it.printStackTrace()
                Assert.fail()
            }
            Unit
        }

        val endHandler = {
            shutdownPermit.release()
            log.debug("Observing : SUCCESS")
        }

        val src = BufferChain("end").generic()
        val target = BufferChain("end").generic()


        for (i in 0..10)
            src.add(i, 1, "value$i")
        src.end()
        src.readPosition(5)
        src.trim(5)

        target.add(0, 5, "Dummy")

        val pipelineFactory = { _: String ->
            Pipeline(
                listOf("STEP"),
                listOf(src, target),
                listOf(
                    SegmentProcessorTest.RandomLineAppenderProcessor(4)
                ),
                endHandler,
                stateObserver
            )
        }

        val pipelineExecutor = PipelineExecutor()
        val id = "id"
        pipelineExecutor.begin(id, pipelineFactory(id))

        shutdownPermit.acquire()
        pipelineExecutor.shutdown()
    }
}