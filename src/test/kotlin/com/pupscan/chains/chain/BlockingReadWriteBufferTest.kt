package com.pupscan.chains.chain

import com.pupscan.chains.SleepStrategy
import com.pupscan.chains.paddedString
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.atomic.AtomicReference
import kotlin.concurrent.thread
import kotlin.math.roundToLong
import kotlin.random.Random

class BlockingReadWriteBufferTest {

    @Test
    fun test_simple_read_to_end() {
        var writeCount = 1000

        val buffer = BlockingReadWriteBuffer("END")

        Producer(buffer, writeCount).run()
        Consumer(buffer, buffer.writeCount).run()
    }

    @Test
    fun test_multiple_positions_read() {
        var writeCount = 30

        val buffer = BlockingReadWriteBuffer("END")
        Producer(buffer, writeCount).run()

        buffer.readPosition(0)
        Assert.assertEquals(0, buffer.readPosition)

        //Read all once
        Consumer(buffer, buffer.writeCount).run()
        Assert.assertEquals(writeCount, buffer.readPosition)

        //Read agagin
        for (i in 1..10) {
            val newPosition = Random.nextInt(0, buffer.writeCount)
            println("------ newPosition = $newPosition")
            buffer.readPosition(newPosition)
            Assert.assertEquals(newPosition, buffer.readPosition)
            Consumer(buffer, writeCount - newPosition).run()
            Assert.assertEquals(writeCount, buffer.readPosition)
        }
        println("------ done")
    }

    @Test
    fun test_trim() {
        var writeCount = 4 * 3
        val halfCount = writeCount / 2
        val quarterCount = writeCount / 4

        val buffer = BlockingReadWriteBuffer("END")
        Producer(buffer, writeCount).run()

        printState(buffer, writeCount)
        Assert.assertEquals(0, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(0, buffer.trimCount)

        Consumer(buffer, buffer.writeCount).run()

        printState(buffer, writeCount)
        Assert.assertEquals(writeCount, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(0, buffer.trimCount)

        buffer.readPosition(halfCount)

        printState(buffer, writeCount)
        Assert.assertEquals(halfCount, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(0, buffer.trimCount)

        try {
            buffer.trim(buffer.readPosition + 1)
            Assert.fail("Should throw an exception because trim is conflicting with readPosition.")
        } catch (ignored: Chain.IllegalReadException) {
        }

        buffer.trim(quarterCount - 1)
        buffer.trim(1)

        printState(buffer, writeCount)
        Assert.assertEquals(halfCount, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(quarterCount, buffer.trimCount)

        buffer.readPosition(quarterCount)

        printState(buffer, writeCount)
        Assert.assertEquals(quarterCount, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(quarterCount, buffer.trimCount)

        Consumer(buffer, halfCount + quarterCount).run()

        printState(buffer, writeCount)
        Assert.assertEquals(writeCount, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(quarterCount, buffer.trimCount)

        buffer.trim(halfCount + quarterCount)

        printState(buffer, writeCount)
        Assert.assertEquals(writeCount, buffer.readPosition)
        Assert.assertEquals(writeCount, buffer.writeCount)
        Assert.assertEquals(writeCount, buffer.trimCount)
    }

    @Test
    fun test_massive_parallel_rw_absence_of_dead_lock() {
        val writeCount = 1000
        val delayMs = 5L

        val buffer = BlockingReadWriteBuffer("END")

        val producer = Producer(buffer, writeCount, SleepStrategy.RandomDuration(delayMs))
        val consumer =
            Consumer(buffer, writeCount, (delayMs * 1.2f).roundToLong(), SleepStrategy.RandomDuration(delayMs), true)

        runParallel(producer, consumer)
    }


    fun printState(buffer: BlockingReadWriteBuffer<String>, count: Int) {
        println(
            "-> writeCount=${buffer.writeCount.paddedString(count)} , " +
                    "readPosition=${buffer.readPosition.paddedString(count)}, " +
                    "trimCount=${buffer.trimCount.paddedString(count)}"
        )
    }

    inner class Consumer(
        val buffer: BlockingReadWriteBuffer<String>,
        val count: Int,
        val readTimeout: Long = 100,
        val sleepStrategy: SleepStrategy = SleepStrategy.Never(),
        val trim: Boolean = false
    ) : Runnable {
        override fun run() {
            (0 until count).forEach { index ->
                val paddedIndex = index.paddedString(count)
                if (trim && Random.nextInt(5) == 0) {
                    //1 time out of 5, trim a random number of items
                    printState(buffer, count)
                    val trimCount = Random.nextInt(buffer.readPosition - buffer.trimCount + 1)
                    buffer.trim(trimCount)
                    println("Trim[$paddedIndex]='$trimCount'")
                }

                sleepStrategy.sleep(index)

                val t = buffer.read(readTimeout) ?: throw Chain.IllegalReadException("index = $index, count = $count")
                println("Read[$paddedIndex]='$t'")
            }
        }
    }


    inner class Producer(
        private val buffer: BlockingReadWriteBuffer<String>, val count: Int,
        val sleepStrategy: SleepStrategy = SleepStrategy.Never()
    ) : Runnable {


        override fun run() {
            (0 until count).forEach { index ->
                sleepStrategy.sleep(index)
                val paddedIndex = index.paddedString(count)
                val value = "item-$paddedIndex"
                buffer.write(value)
                println("Write[$paddedIndex]='$value'")
            }
            buffer.writeEnd()
        }

    }


    private fun runParallel(
        producer: Runnable,
        consumer: Runnable
    ) {
        val error = AtomicReference<Throwable>(null)
        Thread.setDefaultUncaughtExceptionHandler { _, e -> error.set(e); e.printStackTrace() }

        val producerThread = thread {
            Thread.currentThread().name = "Producer"
            producer.run()
        }

        val consumerThread = thread {
            Thread.currentThread().name = "Consumer"
            consumer.run()
        }

        producerThread.join()
        consumerThread.join()

        if (error.get() != null) Assert.fail("UncaughtError during execution.")
    }


}
