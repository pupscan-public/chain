package com.pupscan.chains.chain

import com.pupscan.chains.store.MemoryStore

class RestorableChainTest : AbstractStoredChainTest(factory) {
    companion object {

        val valueEnd = "ValueEnd"
        val store = MemoryStore<Chain.Node<String>>()

        val factory = {
            RestorableChain(BufferChain<String>("END_OF_CHAIN"), valueEnd, store)
        }
    }
}