package com.pupscan.chains.chain

import com.pupscan.chains.printlnTimed
import org.junit.Assert
import org.junit.Test
import kotlin.concurrent.thread
import kotlin.random.Random


abstract class AbstractChainTest(val chainFactory: () -> Chain<String>) {


    val valueGenerator = { index: Int -> "Value$index" }
    val provider = { id: String -> "Value$id" }


    fun randomAdds(chain: Chain<String>, count: Int): Chain<String> {
        var index = 0
        for (i in 0 until count) {
            val sourceCount = Random.nextInt(1, 4)
            chain.add(index, 1 + sourceCount - 1, valueGenerator(i))
            index += sourceCount
        }
        chain.end()
        return chain
    }

    @Test
    fun `Can write and read multiple items`() {
        val chain = chainFactory()
        val count = 10
        (0 until count).forEach { index ->
            chain.add(index, 1, provider(index.toString()))
        }


        Assert.assertEquals(count, chain.count)
        Assert.assertFalse(chain.isEnded())

        chain.end()

        Assert.assertTrue(chain.isEnded())

        Assert.assertEquals(count, chain.lastNode!!.let { it.originIndex + it.originCount })

        chain.sequence().forEachIndexed { index, node ->
            Assert.assertEquals(provider(index.toString()), node.lazy.value)
            println("${index} : ${node.lazy.value}")
        }

        chain.recycle()
    }


    @Test
    fun `Can write and read concurrently`() {
        val chain = chainFactory()
        val produceCount = 10
        val producer = thread {

            (0 until produceCount).forEach { index ->
                Thread.sleep(Random.nextLong(300L))
                chain.add(index, 1, provider(index.toString()))
            }
            chain.end()
        }

        var readCount = 0
        val consumer = thread {
            chain.sequence().forEach {
                printlnTimed(it)
                readCount++
            }
        }

        producer.join()
        consumer.join()

        Assert.assertEquals(produceCount, readCount)

        chain.recycle()
    }

    @Test
    fun `Reading out of bounds should throw an IllegalReadException`() {
        val chain = chainFactory()
        randomAdds(chain, 10)
        try {
            chain.readPosition(20)
            Assert.fail("Chain.IllegalReadException expected.")
        } catch (e: Chain.IllegalReadException) {
        }
        chain.recycle()
    }

    @Test
    fun `Can resume reading to a backward position`() {
        val chain = chainFactory()
        randomAdds(chain, 10)

        chain.readPosition(5)

        var node: Chain.Node<String>?
        do {
            node = chain.read()
            if (node != null) println(node)
        } while (node != null)

        chain.recycle()
    }

    @Test
    fun `Trim is working as expected`() {
        var chain = chainFactory()
        randomAdds(chain, 20)
        chain.readPosition(10)
        chain.trim(5)

        //Test readPosition vs trimCount
        try {
            chain.readPosition(chain.trimCount - 1)
            Assert.fail("Should throw an exception because read is trying to underpass trimCount.")
        } catch (ignored: Chain.IllegalReadException) {
        }
        chain.readPosition(chain.trimCount)
        chain.readPosition(chain.trimCount + 1)
        chain.recycle()

        //Test trimCount vs readPosition
        chain = chainFactory()
        randomAdds(chain, 20)
        chain.readPosition(10)
        try {
            chain.trim(chain.readPosition + 1)
            Assert.fail("Should throw an exception because trim is trying to overpass the readPosition.")
        } catch (ignored: Chain.IllegalReadException) {
        }
        chain.trim(chain.readPosition - 1)
        chain.trim(1)

        var node: Chain.Node<String>?
        do {
            node = chain.read()
            if (node != null) println(node)
        } while (node != null)

        chain.recycle()
    }


}