package com.pupscan.chains

import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random

interface SleepStrategy {
    fun sleep(nextIndex: Int)

    class Never : SleepStrategy {
        override fun sleep(nextIndex: Int) {
        }
    }

    class FixDurationOnlyOnce(val index: Int, val durationMs: Long) : SleepStrategy {
        private var firstConditionMatch = AtomicBoolean(true)
        override fun sleep(nextIndex: Int) {
            if (nextIndex >= index && firstConditionMatch.getAndSet(false)) {
                Thread.sleep(durationMs)
            }
        }
    }

    class FixDuration(val durationMs: Long) : SleepStrategy {

        override fun sleep(nextIndex: Int) {
            Thread.sleep(durationMs)
        }
    }

    class RandomDuration(val maxDurationMs: Long) : SleepStrategy {

        override fun sleep(nextIndex: Int) {
            Thread.sleep(Random.nextLong(maxDurationMs))
        }
    }
}