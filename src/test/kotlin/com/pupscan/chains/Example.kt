package com.pupscan.chains

import com.pupscan.chains.chain.BufferChain
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.chain.generic
import com.pupscan.chains.condition.Condition
import com.pupscan.chains.condition.VerifiedCondition
import com.pupscan.chains.pipeline.Pipeline
import com.pupscan.chains.pipeline.PipelineExecutor
import com.pupscan.chains.processor.SegmentProcessor
import com.pupscan.chains.processor.SegmentProcessorTest
import org.junit.Test
import org.slf4j.LoggerFactory
import java.lang.StringBuilder
import java.util.concurrent.Semaphore
import kotlin.random.Random

class Example {

    val log = LoggerFactory.getLogger(javaClass)
    val STEP1 = "STEP1"
    val STEP2 = "STEP2"
    val steps = listOf(STEP1, STEP2)

    @Test
    fun test() {
        //Will trigger the shutdown on Pipeline success.
        val shutdownPermit = Semaphore(0)

        //Observe the progress of a step of the Pipeline
        val stepObserver = { pipeline: Pipeline, step: String ->
            val processor = pipeline.processorFor(step)
            log.info("Observing : $processor")
        }

        //Handle the success of the last step of the Pipeline
        val endHandler = {
            shutdownPermit.release()
            log.info("Observing : SUCCESS")
        }

        //Our Pipeline components
        //We have 2 steps, so we need 3 Chain and 2 SegmentProcessor
        val id = "pipeline1"
        val chains = listOf(
            newChains(),
            newChains(),
            newChains()
        )
        val processors = listOf(
            processor(),
            processor()
        )

        //Here you can define the Conditions that should be verified before triggering execution of each step.
        //Example : network is up, some other resource is available...
        val conditions = listOf<Condition>(VerifiedCondition(), VerifiedCondition())

        //Create the Pipeline : chain[0] -> STEP1 / processor[0] -> chain[1] -> STEP2 / processor[1] -> chain[2]
        val pipeline = Pipeline(
            steps,
            chains,
            processors,
            endHandler,
            stepObserver,
            conditions
        )

        //Will handle the execution of every Pipeline.
        val pipelineExecutor = PipelineExecutor()

        //Submit our Pipeline
        pipelineExecutor.begin(id, pipeline)

        //Feed the Pipeline (this is equivalent to adding to 'chain0')
        for (i in 1..15)
            pipelineExecutor.add(id, "value$i")
        //End the Pipeline (this is equivalent to ending 'chain0')
        pipelineExecutor.end(id)

        //Use our semaphone to wait for the success
        shutdownPermit.acquire()

        //Shutdown our executor.
        pipelineExecutor.shutdown()
    }

    private fun newChains(): Chain<Any> {
        //Create an in-memory (not persisted) Chain<String> and cast it to Chain<Any> by invoking '.generic()'
        return BufferChain("end").generic()
    }

    private fun processor(): SegmentProcessor {
        //This is our test implementation of a processor. In our case will concatenate up to 2 strings and wait for
        //a random duration so we have a chance to see that both steps are running in parallel.
        return object : SegmentProcessor {
            override fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>> {
                val items = mutableListOf<String>()
                for (i in 0..Random.nextInt(2)) {
                    val node = source.read(sourceTimeoutMs) ?: break
                    items.add(node.lazy.value.toString())
                }
                Thread.sleep(Random.nextLong(50))
                if (items.isEmpty())
                    return 0 to emptyList()
                else
                    return items.size to listOf(items.joinToString(", "))
            }
        }
    }
}