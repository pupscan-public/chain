package com.pupscan.chains.store

class MemoryStoreTest : AbstractStoreTest<String>(store, stringIdProvider, stringComparator) {

    companion object {
        val store = MemoryStore<String>()
    }
}