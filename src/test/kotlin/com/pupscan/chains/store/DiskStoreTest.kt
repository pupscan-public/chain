package com.pupscan.chains.store

import com.pupscan.chains.stream.StringStreamizer
import com.pupscan.chains.TestUtils
import org.junit.Test

class DiskStoreTest : AbstractStoreTest<String>(store, stringIdProvider, stringComparator) {

    companion object {
        val store = DiskStore<String>(TestUtils.testDirectory(DiskStoreTest::class.java), StringStreamizer())

    }
}