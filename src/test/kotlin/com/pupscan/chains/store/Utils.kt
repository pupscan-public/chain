package com.pupscan.chains.store

import java.security.MessageDigest

object Utils {


    fun intSeq(count: Int): Sequence<Int> {
        return (0 until count).asSequence()
    }

    fun stringSeq(count: Int): Sequence<String> {
        return intSeq(count).map {
            "Item$it"
        }
    }

    fun md5(bytes: ByteArray): String {
        return MessageDigest.getInstance("MD5").digest(bytes).joinToString("") { "%02x".format(it) }
    }

}