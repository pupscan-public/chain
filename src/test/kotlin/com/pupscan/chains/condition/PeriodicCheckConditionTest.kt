package com.pupscan.chains.condition

import org.junit.Test
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

class PeriodicCheckConditionTest {

    @Test
    fun test() {
        val verified = AtomicBoolean(false)
        val periodicCheckCondition = PeriodicCheckCondition({ println("Checking"); verified.get() }, 10L)

        val endPermit = Semaphore(0)

        periodicCheckCondition.runWhenVerified(Runnable {
            println("Release ------------")
            endPermit.release()
        })

        thread {
            println("Sleeping")
            Thread.sleep(50)
            println("Set true")
            verified.set(true)
        }

        endPermit.acquire()
        println("Acquired -------------")

        Thread.sleep(100)
    }
}