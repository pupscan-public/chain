package com.pupscan.chains.processor

import com.pupscan.chains.ErrorStrategy
import com.pupscan.chains.SleepStrategy
import com.pupscan.chains.chain.Chain
import kotlin.random.Random

class OneToOneProcessorTest {

    class DecoratingOneToOneProcessor(
        val sleepStrategy: SleepStrategy = SleepStrategy.Never(),
        val errorStrategy: ErrorStrategy = ErrorStrategy.Never()
    ) : OneToOneProcessor<String, String>() {

        override fun process(index: Int, t: String): String {
            errorStrategy.throwError(index)
            sleepStrategy.sleep(index)
            return "[$index]$t"
        }
    }



}