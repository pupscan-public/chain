package com.pupscan.chains.processor

import com.pupscan.chains.ErrorStrategy
import com.pupscan.chains.SleepStrategy
import com.pupscan.chains.chain.BufferChain
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.chain.generic
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.TimeoutException
import kotlin.concurrent.thread

class ChainProcessorTest {
    companion object {

        fun sourceChain(key: String, count: Int): Chain<String> {
            val chain = BufferChain("EOC")
            (0 until count).forEach { chain.add(it, 1, "Item[$key][$it]") }
            chain.end()
            return chain
        }


        fun targetChain(): Chain<Any> {
            return BufferChain("end").generic()
        }

        /**
         * @param failIndex: throw an Exception when this index is reached
         * @param segProcDurationMs: duration of each segmentProcessor iteration in milliseconds
         */
        fun processor(
            id: String,
            count: Int,
            failIndex: Int = Int.MAX_VALUE,
            segProcDurationMs: Long = 0L
        ): ChainProcessor {
            return ChainProcessor(
                "$id-step1",
                ChainProcessorTest.sourceChain(id, count),
                ChainProcessorTest.targetChain(),
                SegmentProcessorTest.RandomLineAppenderProcessor(
                    4, SleepStrategy.FixDuration(segProcDurationMs),
                    ErrorStrategy.OnlyOnce(failIndex, Exception("Fake"))
                )
            )
        }


    }


    @Test
    fun testTimeout() {

        val id = "id"
        val emptyListChain = BufferChain<String>("END")
        val processor = ChainProcessor(
            "$id-step",
            emptyListChain,
            targetChain(),
            SegmentProcessorTest.RandomLineAppenderProcessor(4)
        )

        ChainProcessor.sourceReadTimeOutMs = 1
        processor.run()
        Assert.assertEquals(ChainProcessor.State.ERROR, processor.state)
        Assert.assertTrue(processor.progress.exception is TimeoutException)
    }

    @Test
    fun testFailure() {
        val processor = ChainProcessorTest.processor("id", 20, failIndex = 10)
        processor.run()
        Assert.assertEquals(ChainProcessor.State.ERROR, processor.state)
        Assert.assertTrue(processor.progress.exception != null)
    }

    @Test
    fun testSuccess() {
        val count = 20
        val processor = ChainProcessorTest.processor("id", count)
        processor.run()
        Assert.assertEquals(count, processor.progress.completed)
        Assert.assertEquals(ChainProcessor.State.SUCCESS, processor.state)
        Assert.assertTrue(processor.progress.exception == null)
    }

    @Test
    fun testFailureAndResumeToSuccess() {
        val count = 20
        val processor = ChainProcessorTest.processor("id", count, failIndex = 10)
        processor.run()
        Assert.assertEquals(ChainProcessor.State.ERROR, processor.state)
        Assert.assertTrue(processor.progress.exception != null)

        processor.run()

        Assert.assertEquals(count, processor.progress.completed)
        Assert.assertEquals(ChainProcessor.State.SUCCESS, processor.state)
        Assert.assertTrue(processor.progress.exception == null)
    }

    @Test
    fun testInterrupt() {
        val processor = ChainProcessorTest.processor(
            "id"
            , 40
            , segProcDurationMs = 500L
        )

        val processorThread = thread {
            try {
                processor.run()
            } catch (e: InterruptedException) {

            }
        }

        Thread.sleep(500)
        processorThread.interrupt()

        processorThread.join(1000)

        if (processorThread.isAlive) Assert.fail()
        Assert.assertFalse(processor.target.isEnded())
    }
}